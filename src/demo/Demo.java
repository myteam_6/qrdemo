package demo;

import qrcode.QRCodeUtil;

public class Demo {
    public static void main(String[] args) {
//        String line = "我是传奇老师";
        String line = "http://doc.canglaoshi.org";
        try {
//            QRCodeUtil.encode(line,"./qr.jpg");
            //参数1:二维码内容 参数2:二维码中间的logo图 参数3:二维码生成的位置 参数4:logo图是否需要压缩尺寸
            QRCodeUtil.encode(line,"./logo.jpg","./qr.jpg",true);
            System.out.println("二维码生成完毕!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
